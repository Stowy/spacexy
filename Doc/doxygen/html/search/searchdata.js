var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstu",
  1: "abcefghilmpst",
  2: "ceu",
  3: "abcefghilmpst",
  4: "acdeghilmopqrsu",
  5: "abcdefghilmnoprst",
  6: "ghi",
  7: "acgiklmps",
  8: "abcdghimnoprst",
  9: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Events"
};

