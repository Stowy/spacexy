/**
 * @file Health.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Health class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Script to put on gameobjects that must have health like the player or enemies.
/// </summary>
public class Health : MonoBehaviour
{
    public int maxHealthPointsParameter = 15;
    public int maxArmorPointsParameter = 15;
    public bool isAlive = true;

    /// <summary>
    /// Times it takes to destroy the gameobject when he gets killed.
    /// </summary>
    public float destroyTime = 0;

    public bool destroyWhenKilled = true;

    public delegate void HurtCallback(int healthPoint);

    public int HealthPoints { get; private set; }

    public int ArmorPoints { get; private set; }

    public bool IsAlive { get => isAlive; private set => isAlive = value; }

    /// <summary>
    /// Times it takes to destroy the gameobject when he gets killed.
    /// </summary>
    public float DestroyTime { get => destroyTime; private set => destroyTime = value; }

    public HurtCallback OnHurt { get; set; }

    public int MaxHealthPoints { get; set; }

    public int MaxArmorPoints { get; set; }

    private void Awake()
    {
        MaxHealthPoints = maxHealthPointsParameter;
        MaxArmorPoints = maxArmorPointsParameter;

        HealthPoints = MaxHealthPoints;
        ArmorPoints = MaxArmorPoints;
    }

    /// <summary>
    /// Reduces the health of the gameobject and kills it if health <= 0.
    /// </summary>
    /// <param name="ammount">The ammount of health to substract. Defaults to 1.</param>
    /// <param name="destroy">If the gameobject should be destroyed when he gets killed.</param>
    public void ReduceHealth(int ammount = 1)
    {
        if (ArmorPoints > ammount)
        {
            ArmorPoints -= ammount;
        }
        else
        {
            ammount -= ArmorPoints;
            ArmorPoints = 0;
            HealthPoints -= ammount;
        }

        if (HealthPoints <= 0)
        {
            IsAlive = false;
        }

        OnHurt?.Invoke(HealthPoints);

        if (!IsAlive && destroyWhenKilled)
        {
            Destroy(gameObject, DestroyTime);
        }
    }
}