/**
 * @file Shooting.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Shooting class. Inspired by this video: https://www.youtube.com/watch?v=LNLVOjbrQj4
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Handles the shooting.
/// </summary>
public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 20f;

    private PlayerControls controls;

    private void Awake()
    {
        controls = new PlayerControls();
        controls.Gameplay.Shoot.performed += ctx => Shoot();
    }

    /// <summary>
    /// Shoots a bullet.
    /// </summary>
    private void Shoot()
    {
        if (Time.timeScale != 0f)
        {
            // Instantiate the bullet
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

            // Tell the bullet the layer of this gameobject to ignore collisions with it
            Bullet script = bullet.GetComponent<Bullet>();
            script.originLayerId = gameObject.layer;

            // Add an on hit event.
            script.OnHit += OnHit;

            // Move the bullet.
            Rigidbody2D rbBullet = bullet.GetComponent<Rigidbody2D>();
            rbBullet.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
        }
    }

    private void OnHit(bool isAlive)
    {
        if (!isAlive)
        {
            MissionBehaviour missionBehaviour = gameObject.GetComponent<MissionBehaviour>();
            missionBehaviour?.EnemyKilled();
        }
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}