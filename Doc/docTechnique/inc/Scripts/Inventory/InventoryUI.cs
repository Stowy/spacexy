/**
* @file InventoryUI.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the InventoryUI class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using CodeMonkey.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// MonoBehaviour that controls the UI for the inventory.
/// </summary>
public class InventoryUI : MonoBehaviour
{
    public GameObject itemSlotContainer;
    public GameObject itemSlotTemplate;
    public GameObject armorItem;
    public TextMeshProUGUI moneyText;
    public float itemSlotSpacing = 0.3f;
    public float itemSlotSize = 75f;
    public int numberItemsInOneLine = 7;
    public string monetarySimbol = "$";

    private Inventory inventory;

    public Inventory Inventory
    {
        get => inventory;
        set
        {
            inventory = value;
            inventory.OnInventoryUpdate += Inventory_OnInventoryUpdate;
            RefreshInventoryItems();
        }
    }

    private void Awake()
    {
        ButtonUI button = armorItem.GetComponent<ButtonUI>();
        button.ClickFunc = () =>
        {
            if (inventory.ArmorItem != null)
            {
                Item item = Inventory.ArmorItem;
                Inventory.ArmorItem = null;
                Inventory.AddItem(item);
                RefreshInventoryItems();
            }
        };
    }

    private void Inventory_OnInventoryUpdate(object sender, System.EventArgs e)
    {
        RefreshInventoryItems();
    }

    private void RefreshInventoryItems()
    {
        int x = 0;
        int y = 0;

        // Update the armor item
        GameObject armorItemImage = armorItem.transform.Find("Image").gameObject;
        if (inventory.ArmorItem == null)
        {
            armorItemImage.SetActive(false);
        }
        else
        {
            armorItemImage.SetActive(true);
            armorItemImage.GetComponent<Image>().sprite = inventory.ArmorItem.Sprite;
        }

        // Update the money
        moneyText.SetText($"{Inventory.Money} {monetarySimbol}");

        // Empty the inventory
        foreach (Transform child in itemSlotContainer.transform)
        {
            if (child.gameObject != itemSlotTemplate)
            {
                Destroy(child.gameObject);
            }
        }

        foreach (Item item in Inventory.Items)
        {
            // Instantiate the item
            GameObject itemSlot = Instantiate(itemSlotTemplate, itemSlotContainer.transform);

            // Set the item active
            itemSlot.SetActive(true);

            // Set click function
            ButtonUI button = itemSlot.GetComponent<ButtonUI>();
            button.ClickFunc = () =>
            {
                inventory.UseItem(item);
            };

            // Move the item to it's position
            itemSlot.GetComponent<RectTransform>().anchoredPosition = new Vector2(x * (itemSlotSize + itemSlotSpacing), y * (itemSlotSize + itemSlotSpacing));

            // Change the image of the item
            Image image = itemSlot.transform.Find("Image").GetComponent<Image>();
            image.sprite = item.Sprite;

            // Set the amount
            TextMeshProUGUI uiText = itemSlot.transform.Find("Amount").GetComponent<TextMeshProUGUI>();
            if (item.Amount > 1)
            {
                uiText.SetText(item.Amount.ToString());
            }
            else
            {
                uiText.SetText("");
            }

            // Increment the position index
            x++;
            if (x > numberItemsInOneLine)
            {
                x = 0;
                y++;
            }
        }
    }
}