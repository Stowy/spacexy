/**
* @file ItemAssets.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ItemAssets class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Singleton class that contains reference to all the necessary assets related to the items.
/// </summary>
public class ItemAssets : Singleton<ItemAssets>
{
    public GameObject itemWorldPrefab;
    public Sprite silverSprite;
    public Sprite platinumSprite;
    public Sprite armorUpgradeSprite;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}