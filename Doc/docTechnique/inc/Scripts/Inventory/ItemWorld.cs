/**
* @file ItemWorld.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ItemWorld class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Item in the world that can be picked up.
/// </summary>
public class ItemWorld : MonoBehaviour
{
    private SpriteRenderer sp;
    private Item item;

    public Item Item
    {
        get => item;
        set
        {
            item = value;
            sp.sprite = value.Sprite;
        }
    }

    /// <summary>
    /// Spawns an Item on the scene.
    /// </summary>
    /// <param name="position">Position of the item.</param>
    /// <param name="item">Item to spawn.</param>
    /// <returns>Returns the ItemWorld component of the spawed item.</returns>
    public static ItemWorld SpawnItemWorld(Vector2 position, Item item)
    {
        GameObject spawedItem = Instantiate(ItemAssets.Instance.itemWorldPrefab, position, Quaternion.identity);

        ItemWorld itemWorld = spawedItem.GetComponent<ItemWorld>();
        if (itemWorld != null)
        {
            itemWorld.Item = item;
        }

        return itemWorld;
    }

    private void Awake()
    {
        sp = GetComponent<SpriteRenderer>();
    }
}