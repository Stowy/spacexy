/**
* @file ExtentionMethods.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ExtentionMethods class.
* @version 1.0
* @date 26.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

namespace Extensions
{
    /// <summary>
    /// A set of extention methods I use in my application.
    /// </summary>
    public static class ExtensionMethods
    {
        private const int DEFAULT_FLOAT_PRECISION = 1000;
        private const int DEFAULT_PRECISION_DIRECTION = 1000000;

        /// <summary>
        /// Gets a random float between min and max with a specified precision.
        /// </summary>
        public static float NextFloat(this System.Random rnd, int min, int max, int precision = DEFAULT_FLOAT_PRECISION)
        {
            return (float)rnd.Next(min * precision, max * precision) / precision;
        }

        /// <summary>
        /// Returns a normalised random direction.
        /// </summary>
        public static Vector2 GetRandomDirection(this System.Random rnd, int precision = DEFAULT_PRECISION_DIRECTION)
        {
            float rndValueX = rnd.NextFloat(-1, 1, precision);
            float rndValueY = rnd.NextFloat(-1, 1, precision);
            return new Vector2(rndValueX, rndValueY).normalized;
        }

        /// <summary>
        /// Gets the angle to the provided point.
        /// </summary>
        /// <param name="vector">Starting point</param>
        /// <param name="destination">Destination point</param>
        public static float AngleToPoint(this Vector2 vector, Vector2 destination)
        {
            Vector2 lookDirection = destination - vector;
            float rotZ = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
            return rotZ;
        }
    }
}