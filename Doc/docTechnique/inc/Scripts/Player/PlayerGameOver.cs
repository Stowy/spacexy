/**
 * @file PlayerGameOver.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PlayerGameOver class.
 * @version 1.0
 * @date 28.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Handles the game over for the player.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerGameOver : MonoBehaviour
{
    public GameObject gameOverUI;

    /// <summary>
    /// Checks if the player is dead, and if yes, put the game to the GameOver state.
    /// </summary>
    /// <param name="healthPoint">Health points of the player.</param>
    private void CheckDead(int healthPoint)
    {
        if (healthPoint <= 0)
        {
            GameManager.Instance.State = GameManager.GameState.GameOver;
            gameOverUI.SetActive(true);
        }
    }

    private void Awake()
    {
        // Invoke CheckDead when the player is hurt
        Health health = GetComponent<Health>();
        health.OnHurt = CheckDead;
    }
}