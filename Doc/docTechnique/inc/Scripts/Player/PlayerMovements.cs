/**
 * @file PlayerMovements.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PlayerMovements class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Class used by the PlayerMovementsBehaviour to compute the movements.
/// </summary>
public class PlayerMovements
{
    private const float DEFAULT_SPEED_MODIFIER = 0;
    public readonly int rotationOffset;

    public PlayerMovements(float playerSpeed, float afterburnerSpeed)
    {
        PlayerSpeed = playerSpeed;
        AfterburnerSpeed = afterburnerSpeed;
        SpeedModifier = DEFAULT_SPEED_MODIFIER;
    }

    public float PlayerSpeed { get; private set; }
    public float AfterburnerSpeed { get; private set; }
    public float SpeedModifier { get; private set; }

    /// <summary>
    /// Get the movement that should be applied to the rigidbody.
    /// </summary>
    /// <param name="movement">Describes the movement.</param>
    /// <returns>Returns the movement that should be applied to the rigidbody.</returns>
    public Vector2 GetMovement(Vector2 movement)
    {
        return movement * (PlayerSpeed + SpeedModifier);
    }

    /// <summary>
    /// Makes the rigidbody go faster.
    /// </summary>
    public void StartAfterburner()
    {
        SpeedModifier = AfterburnerSpeed;
    }

    /// <summary>
    /// Stops the afterburner.
    /// </summary>
    public void StopAfterburner()
    {
        SpeedModifier = 0;
    }
}