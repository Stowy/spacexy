/**
* @file Asteroid.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Asteroid class.
* @version 1.0
* @date 02.06.2020
*
* @copyright CFPT (c) 2020
*
*/

using Extensions;
using UnityEngine;

/// <summary>
/// MonoBehaviour for an Asteroid that drops items on destroy.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(Health))]
public class Asteroid : MonoBehaviour
{
    public float speed = 1f;
    public int minLoot = 0;
    public int maxLoot = 5;
    public float minLootRange = -1f;
    public float maxLootRange = 1f;
    public Item.ItemType lootType;

    private Rigidbody2D rb;
    private Health health;
    private System.Random rnd;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rnd = new System.Random(Random.Range(0, 10000));
        health = GetComponent<Health>();
        health.OnHurt = OnHurt;
    }

    private void Start()
    {
        rb.AddForce(rnd.GetRandomDirection() * speed, ForceMode2D.Impulse);
    }

    private void OnHurt(int hp)
    {
        if (!health.IsAlive)
        {
            SpawnLoot();
        }
    }

    /// <summary>
    /// Spawns the loot.
    /// </summary>
    private void SpawnLoot()
    {
        int nbrLoot = Random.Range(0, 5);
        for (int i = 0; i < nbrLoot; i++)
        {
            float xOffset = Random.Range(minLootRange, maxLootRange);
            float yOffset = Random.Range(minLootRange, maxLootRange);
            Vector2 lootPosition = new Vector2(transform.position.x + xOffset, transform.position.y + yOffset);
            ItemWorld.SpawnItemWorld(lootPosition, new Item(lootType, 1));
        }
    }
}