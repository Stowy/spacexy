/**
 * @file GameOverButtonsActions.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the GameOverButtonsActions class.
 * @version 1.0
 * @date 28.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Contains the methods for the game over buttons.
/// </summary>
public class GameOverButtonsActions : MonoBehaviour
{
    /// <summary>
    /// Retry button action.
    /// </summary>
    public void Retry()
    {
        GameManager.Instance.Restart();
    }

    /// <summary>
    /// Quit button action.
    /// </summary>
    public void Quit()
    {
        Application.Quit();
    }
}