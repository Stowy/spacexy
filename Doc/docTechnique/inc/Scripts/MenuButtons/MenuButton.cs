/**
 * @file MenuButton.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the MenuButton class. Inspired by this video: https://www.youtube.com/watch?v=vqZjZ6yv1lA
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Manages the animations for a menu button.
/// </summary>
public class MenuButton : MonoBehaviour
{
    public int thisIndex;

    private PlayerControls controls;
    private Animator animator;
    private MenuButtonsController menuButtonsController;

    private void Awake()
    {
        controls = new PlayerControls();
        animator = GetComponent<Animator>();
        menuButtonsController = GetComponentInParent<MenuButtonsController>();
    }

    // Update is called once per frame
    private void Update()
    {
        // Check if this button is selected
        if (menuButtonsController.selectedButtonIndex == thisIndex)
        {
            // Tell the animator this button is selected
            animator.SetBool("IsSelected", true);
            if (controls.MainMenu.Select.ReadValue<float>() > 0)
            {
                animator.SetBool("IsPressed", true);
            }
            else if (animator.GetBool("IsPressed"))
            {
                animator.SetBool("IsPressed", false);
            }
        }
        else
        {
            animator.SetBool("IsSelected", false);
        }
    }

    private void OnEnable()
    {
        controls.MainMenu.Enable();
    }

    private void OnDisable()
    {
        controls.MainMenu.Disable();
    }
}