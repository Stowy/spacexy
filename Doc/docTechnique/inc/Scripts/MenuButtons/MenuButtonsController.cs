/**
 * @file MenuButtonsController.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the MenuButtonsController class. Inspired by this video: https://www.youtube.com/watch?v=vqZjZ6yv1lA
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles the selection of buttons for the main menu.
/// </summary>
public class MenuButtonsController : MonoBehaviour
{
    public int selectedButtonIndex = 0;
    public int maxButtonIndex;
    public Button[] buttons;

    private bool keyDown;

    private PlayerControls controls;
    private Vector2 direction;

    private void Awake()
    {
        // Create a new PlayerControls
        controls = new PlayerControls();

        //Get the movement from the input
        controls.MainMenu.Directions.performed += ctx => direction = ctx.ReadValue<Vector2>();
        controls.MainMenu.Directions.canceled += ctx => direction = Vector2.zero;

        controls.MainMenu.Select.performed += ctx => Click();
    }

    private void Update()
    {
        // Check if the user inputed something
        if (direction.y != 0)
        {
            // Check if the key hasn't been pressed before
            if (!keyDown)
            {
                if (direction.y < 0) // Press down
                {
                    selectedButtonIndex++;
                    if (selectedButtonIndex > maxButtonIndex)
                    {
                        selectedButtonIndex = 0;
                    }
                }
                else if (direction.y > 0) // Press up
                {
                    selectedButtonIndex--;
                    if (selectedButtonIndex < 0)
                    {
                        selectedButtonIndex = maxButtonIndex;
                    }
                }
            }

            keyDown = true;
        }
        else
        {
            keyDown = false;
        }
    }

    private void Click()
    {
        buttons[selectedButtonIndex].onClick.Invoke();
    }

    private void OnEnable()
    {
        controls.MainMenu.Enable();
    }

    private void OnDisable()
    {
        controls.MainMenu.Disable();
    }
}