/**
* @file PlanetPrice.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the PlanetPrice class.
* @version 1.0
* @date 04.04.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Class to put on planets to describe what they sell and at what price.
/// </summary>
public class PlanetPrice : MonoBehaviour
{
    public ItemPrice[] prices;
}