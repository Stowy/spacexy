/**
* @file PlanetInteractionBehaviour.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the PlanetInteractionBehaviour class.
* @version 1.0
* @date 04.04.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Behaviour to put on the gameobject that needs to interact with the planets.
/// </summary>
[RequireComponent(typeof(InventoryBehaviour))]
public class PlanetInteractionBehaviour : MonoBehaviour
{
    private const string PLANET_TAG = "Planet";
    private const string STATION_TAG = "Station";

    public GameObject planetInteraction;
    public GameObject selectInteraction;

    public GameObject buyMenu;
    public GameObject itemBuyTemplate;
    public GameObject itemsBuy;

    public GameObject sellMenu;
    public GameObject itemSellTemplate;
    public GameObject itemsSell;

    public GameObject missionsMenu;

    public GameObject backButton;

    private bool isInPlanet;
    private PlayerControls controls;

    public bool ShowInteraction { get; set; }
    public ItemPrice[] ItemPrices { get; private set; }
    public Mission MissionOnPlanet { get; private set; } = null;
    

    private void Awake()
    {
        controls = new PlayerControls();

        controls.Gameplay.Interact.performed += Interact_performed;
    }

    private void Interact_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (isInPlanet)
        {
            if (!ShowInteraction)
            {
                ShowInteractSelect();
            }
            else
            {
                HideInteractSelect();
            }
        }
    }

    /// <summary>
    /// Removes all the items from the buy item menu.
    /// </summary>
    public void EmptyBuyItemsMenu()
    {
        foreach (Transform child in itemsBuy.transform)
        {
            if (child.gameObject != itemBuyTemplate)
            {
                Destroy(child.gameObject);
            }
        }
    }

    /// <summary>
    /// Removes all the items from the sell item menu.
    /// </summary>
    public void EmptySellItemsMenu()
    {
        foreach (Transform child in itemsSell.transform)
        {
            if (child.gameObject != itemSellTemplate)
            {
                Destroy(child.gameObject);
            }
        }
    }

    /// <summary>
    /// Show the interaction selection menu.
    /// </summary>
    public void ShowInteractSelect()
    {
        buyMenu.SetActive(false);
        sellMenu.SetActive(false);
        missionsMenu.SetActive(false);
        ShowInteraction = true;
        Time.timeScale = 0f;
        planetInteraction.SetActive(true);
        selectInteraction.SetActive(true);
        backButton.SetActive(false);
    }

    /// <summary>
    /// Hide the interaction selection menu.
    /// </summary>
    public void HideInteractSelect()
    {
        ShowInteraction = false;
        Time.timeScale = 1f;
        planetInteraction.SetActive(false);
        selectInteraction.SetActive(false);
        backButton.SetActive(false);
        EmptyBuyItemsMenu();
        EmptySellItemsMenu();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Active les infos sur les planètes
        string tag = collision.gameObject.tag;
        if (tag == PLANET_TAG || tag == STATION_TAG)
        {
            ItemPrices = collision.GetComponent<PlanetPrice>().prices;
            MissionOnPlanet = collision.GetComponent<MissionGiver>().mission;
            isInPlanet = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        string tag = collision.gameObject.tag;
        if (tag == PLANET_TAG || tag == STATION_TAG)
        {
            ItemPrices = null;
            MissionOnPlanet = null;
            isInPlanet = false;
        }
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}