/**
 * @file EnemyAI.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the EnemyAI class. Inspired by this video: https://www.youtube.com/watch?v=db0KWYaWfeM
 * @version 1.0
 * @date 26.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using Extensions;
using UnityEngine;

/// <summary>
/// Class used by the EnemyAIBehaviour class to compute the AI of an enemy.
/// </summary>
internal class EnemyAI
{
    private const int FLOAT_PRECISION = 10000;
    private const int DEFAULT_MIN_MOVEMENT_RANGE = 1;
    private const int DEFAULT_MAX_MOVEMENT_RANGE = 10;
    private const float DEFAULT_TARGET_RANGE = 10;

    public enum States
    {
        Roaming,
        ChaseTarget
    }

    private readonly System.Random rnd;
    private readonly int minMovementRange;
    private readonly int maxMovementRange;
    private readonly float targetRange;

    public EnemyAI(System.Random random, Vector2 startPosition, int minMovementRange, int maxMovementRange, float targetRange)
    {
        rnd = random;
        State = States.Roaming;
        StartPosition = startPosition;
        this.minMovementRange = minMovementRange;
        this.maxMovementRange = maxMovementRange;
        this.targetRange = targetRange;
    }

    public EnemyAI(System.Random random, Vector2 startPosition) :
        this(random, startPosition, DEFAULT_MIN_MOVEMENT_RANGE, DEFAULT_MAX_MOVEMENT_RANGE, DEFAULT_TARGET_RANGE)
    {
        // Do nothing
    }

    /// <summary>
    /// The starting position of the enemy.
    /// </summary>
    public Vector2 StartPosition { get; private set; }

    /// <summary>
    /// Returns a random roam position close to the start position.
    /// </summary>
    public Vector2 RoamPosition
    {
        get
        {
            return StartPosition + (rnd.GetRandomDirection() * rnd.NextFloat(minMovementRange, maxMovementRange, FLOAT_PRECISION));
        }
    }

    public States State { get; set; }

    /// <summary>
    /// Look if the enemy is in the range of the player.
    /// </summary>
    /// <param name="enemyPosition">Position of the enemy.</param>
    public void FindTarget(Vector2 enemyPosition)
    {
        if (Vector2.Distance(enemyPosition, PlayerMovementsBehaviour.Instance.transform.position) < targetRange)
        {
            State = States.ChaseTarget;
        }
    }
}