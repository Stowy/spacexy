/**
 * @file EnemyAINeutralBehaviour.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the EnemyAINeutralBehaviour class.
 * @version 1.0
 * @date 27.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using Extensions;
using UnityEngine;

/// <summary>
/// Behaviour class using the EnemyAI class to compute the AI.
/// This enemey only rushes on the player if he attacks him.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D), typeof(Health))]
public class EnemyAINeutralBehaviour : MonoBehaviour
{
    private const float REACHED_POSITION_DISTANCE = 1f;

    [SerializeField]
    private float roamSpeed = 1f;

    [SerializeField]
    private float chaseTargetSpeed = 5f;

    [SerializeField]
    private float maxSpeed = 10f;

    [SerializeField]
    private int minRangeRoam = 1;

    [SerializeField]
    private int maxRangeRoam = 10;

    [SerializeField]
    private int targetRange = 10;

    [SerializeField]
    private int rotationOffset = -90;

    [SerializeField]
    private int dammage = 5;

    [SerializeField]
    private float knockbackForce = 2f;

    private Rigidbody2D rb;
    private EnemyAI enemyAI;
    private System.Random rnd;

    private Vector2 roamPosition;
    private Vector2 roamDirection;

    public Vector2 RoamPosition
    {
        get => roamPosition;
        set
        {
            roamPosition = value;
            roamDirection = (RoamPosition - new Vector2(transform.position.x, transform.position.y)).normalized;
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        // We have to put a random seed generated with unity's random to prevent the enemies to move all to the same direction
        rnd = new System.Random(Random.Range(0, 10000));
        enemyAI = new EnemyAI(rnd, transform.position, minRangeRoam, maxRangeRoam, targetRange);

        Health health = GetComponent<Health>();
        health.OnHurt = OnHurt;
    }

    private void Start()
    {
        RoamPosition = enemyAI.RoamPosition;
    }

    private void FixedUpdate()
    {
        if (enemyAI.State == EnemyAI.States.Roaming)
        {
            // Move the enemy to the roam position
            float roamDistance = Vector2.Distance(RoamPosition, transform.position);
            float clampedSpeed = Mathf.Clamp(roamDistance * roamSpeed, 0f, maxSpeed);
            rb.velocity = roamDirection * clampedSpeed;
            rb.rotation = RoamPosition.AngleToPoint(transform.position) + rotationOffset;

            if (Vector2.Distance(transform.position, RoamPosition) < REACHED_POSITION_DISTANCE)
            {
                // Reached roam position
                RoamPosition = enemyAI.RoamPosition;
            }
        }
        else if (enemyAI.State == EnemyAI.States.ChaseTarget)
        {
            Vector2 playerPosition = PlayerMovementsBehaviour.Instance.transform.position;
            Vector2 playerDirection = (playerPosition - new Vector2(transform.position.x, transform.position.y)).normalized;
            rb.velocity = playerDirection * chaseTargetSpeed;
            rb.rotation = playerPosition.AngleToPoint(transform.position) + rotationOffset;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        // Reduces the health of the object touched if it has some
        Health colHeath = col.gameObject.GetComponent<Health>();
        colHeath?.ReduceHealth(dammage);

        // Knockback
        Rigidbody2D rb = col.gameObject.GetComponent<Rigidbody2D>();
        if (rb != null)
        {
            Vector2 force = (col.transform.position - transform.position).normalized * knockbackForce;
            rb.AddForce(force, ForceMode2D.Impulse);
        }
    }

    private void OnHurt(int healthPoint)
    {
        enemyAI.State = EnemyAI.States.ChaseTarget;
    }
}