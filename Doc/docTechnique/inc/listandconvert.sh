#!/bin/bash
# Removes the BOM of all CSharp files and lists them in a file.

shopt -s -o nounset

declare FILES
declare FILE_NO_EXTENTION
declare FILE
declare -i I

# Remove meta files
echo "Deleting meta files..."
find ./Scripts -type f -name '*.meta' -delete 

# Get all the CSharp files
FILES=$(find ./Scripts -type f -name '*.cs')

# Convert the files to UTF8 without BOM
echo "Converting C# files in UTF8 without BOM..."
sed -i $'1s/^\uFEFF//' $FILES

echo "Listing files in list.txt..."
I=0
for FILE in $FILES; do
    # Extract the filename without extention and without the './Scripts/'
    FILE_NO_EXTENTION=$(echo "${FILE:10}" | cut -f 1 -d '.')
    echo "$FILE_NO_EXTENTION"
    if test "$I" -eq 0; then
        echo -n "$FILE_NO_EXTENTION" > list.txt
    else
        echo -n ", $FILE_NO_EXTENTION" >> list.txt
    fi
    I+=1
done

echo "Content of list.txt:"
cat list.txt