# SpaceXY

Mon travail pratique individuel a commencé le 25 mai 2020 avec la réception du cahier des charges du projet.
Il s'agit de faire un jeu dans l'espace où l’on contrôle un vaisseau qui doit accomplir différentes missions.
Le joueur peut se promener dans le système et acheter et vendre des ressources sur les différentes planètes et stations.
Il peut également se battre contre des ennemis qui peuvent être neutres ou agressifs.
J'avais à ma disposition le prototype Starja réalisé par mon prof de TPI.
Le jeu est fait sur Unity et donc en C#.
Comme je suis un 3 + 2, j'ai 80 heures réparti sur 10 jours pour faire ce TPI. Elles ont ensuite été étendues à 88 heures sur 11 jours afin que les 3 + 2 aient la même durée de TPI que les autres participants.

## Ce que j'ai appris

- Système d'inventaire
- Système de quêtes
- Humble object pattern
- Machine d'état appliqué à une "IA" (les énemis)
