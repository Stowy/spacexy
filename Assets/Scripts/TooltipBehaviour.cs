﻿/**
* @file TooltipBehaviour.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the TooltipBehaviour class.
* @version 1.0
* @date 02.06.2020
*
* @copyright CFPT (c) 2020
*
*/

using TMPro;
using UnityEngine;

/// <summary>
/// Add this behaviour to a gameobject that needs to see the tooltip on planets.
/// </summary>
public class TooltipBehaviour : MonoBehaviour
{
    private const string PLANET_TAG = "Planet";

    public GameObject tooltip;
    public TextMeshProUGUI textTooltip;

    private void Start()
    {
        tooltip.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Active les infos sur les planètes
        if (collision.gameObject.tag == PLANET_TAG)
        {
            tooltip.SetActive(true);
            string tooltipText = $"{collision.gameObject.GetComponent<PlanetDescription>()?.Description}Press F to interact.";
            textTooltip.SetText(tooltipText);
            tooltip.transform.position = collision.gameObject.transform.position;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == PLANET_TAG)
        {
            tooltip.SetActive(false);
        }
    }
}