﻿/**
 * @file Bullet.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the Bullet class. Inspired by this video: https://www.youtube.com/watch?v=LNLVOjbrQj4
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using System;
using UnityEngine;

/// <summary>
/// A bullet that hurts and disapear on collision.
/// This bullet ignores the layer of the shooter, so it has to be on a different layer than the target.
/// </summary>
public class Bullet : MonoBehaviour
{
    /// <summary>
    /// Effect to instantiate when the bullet collides.
    /// </summary>
    public GameObject hitEffect;

    /// <summary>
    /// The bullet will not collide with that layer.
    /// </summary>
    public int originLayerId = 0;

    public int dammage = 5;

    public Action<bool> OnHit;

    private void Start()
    {
        Destroy(gameObject, 5);
        Physics2D.IgnoreLayerCollision(gameObject.layer, originLayerId);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        // Show the hit effect if there is one
        if (hitEffect != null)
        {
            GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
            Destroy(effect, 5f);
        }

        // Reduces the health of the object touches if it has some
        Health colHeath = col.GetComponent<Health>();
        colHeath?.ReduceHealth(dammage);

        OnHit?.Invoke(colHeath == null || colHeath.IsAlive);

        // Destroy the bullet
        Destroy(gameObject);
    }
}