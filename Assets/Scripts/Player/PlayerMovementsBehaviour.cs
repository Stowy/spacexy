﻿/**
 * @file PlayerMovementsBehaviour.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PlayerMovementsBehaviour class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// Handles the movements of the player. Uses the PlayerMovements class to compute the movements.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovementsBehaviour : MonoBehaviour
{
    public Camera cam;

    public int rotationOffset = 0;
    public float playerSpeed = 2f;
    public float afterburnerSpeed = 10f;

    private Rigidbody2D rb;
    private PlayerControls controls;
    private Vector2 mousePosition;
    private Vector2 movement;
    private Vector2 oldMovement;

    private PlayerMovements playerMovements;

    public static PlayerMovementsBehaviour Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        rb = GetComponent<Rigidbody2D>();
        controls = new PlayerControls();
        playerMovements = new PlayerMovements(playerSpeed, afterburnerSpeed);

        //Get the movement from the input
        controls.Gameplay.Move.performed += ctx => movement = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => movement = Vector2.zero;

        //Reads if the player wants to afterburn
        controls.Gameplay.Afterburner.started += ctx => playerMovements.StartAfterburner();
        controls.Gameplay.Afterburner.canceled += ctx => playerMovements.StopAfterburner();
    }

    private void Update()
    {
        // Get the position of the mouse in world point
        mousePosition = cam.ScreenToWorldPoint(Mouse.current.position.ReadValue());
    }

    private void FixedUpdate()
    {
        // Moves the player.
        if (movement != oldMovement || movement != Vector2.zero)
        {
            rb.velocity = playerMovements.GetMovement(movement);
        }

        // Rotates the player to the mouse position
        rb.rotation = mousePosition.AngleToPoint(rb.position) + rotationOffset;

        oldMovement = new Vector2(movement.x, movement.y);
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}