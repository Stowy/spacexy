﻿/**
 * @file PauseMenu.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PauseMenu class. Inspired by this video: https://www.youtube.com/watch?v=JivuXdrIHK0
 * @version 1.0
 * @date 27.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles everything related to the pause menu.
/// </summary>
public class PauseMenu : MonoBehaviour
{
    public static bool isPaused = false;

    public GameObject pauseMenuUI;

    private PlayerControls controls;

    private void Awake()
    {
        controls = new PlayerControls();

        controls.Gameplay.Pause.performed += Pause_performed;
    }

    private void Pause_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        if (isPaused)
        {
            Resume();
        }
        else
        {
            Pause();
        }
    }

    /// <summary>
    /// Pauses the game.
    /// </summary>
    private void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    /// <summary>
    /// Resumes the game.
    /// </summary>
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    /// <summary>
    /// Saves the game.
    /// </summary>
    public void Save()
    {
        SaveHandler.Instance.Save();
        Resume();
    }

    /// <summary>
    /// Goes back to the main menu.
    /// </summary>
    public void GoToMainMenu()
    {
        Time.timeScale = 1f;
        // Load the next scene
        SceneManager.LoadScene(0);
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}