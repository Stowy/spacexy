﻿/**
 * @file MainMenuButtonsActions.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the MainMenuButtonsActions class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contains the methods that will be executed by the main menu buttons.
/// </summary>
public class MainMenuButtonsActions : MonoBehaviour
{
    /// <summary>
    /// New Game button.
    /// </summary>
    public void LoadNewGame()
    {
        // Load the next scene
        SceneManager.LoadScene(1);
        GameManager.Instance.State = GameManager.GameState.InGame;
    }

    /// <summary>
    /// Loads the game.
    /// </summary>
    public void Continue()
    {
        SceneManager.LoadScene(1);
        GameManager.Instance.State = GameManager.GameState.LoadSave;
    }

    /// <summary>
    /// Quit button.
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }
}