﻿/**
 * @file FixAltF4.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the FixAltF4 class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;

/// <summary>
/// Fixes the crash that happens when you alt-f4 the application when testing.
/// </summary>
public class FixAltF4 : MonoBehaviour
{
    /// <summary>
    /// Use this for initialization
    /// </summary>
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Sent to all the gameobjects before the application is quit.
    /// </summary>
    private void OnApplicationQuit()
    {
        if (!Application.isEditor)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}