﻿/**
* @file Inventory.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Inventory class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Xml.Serialization;

/// <summary>
/// Inventory class that contains a list of Items.
/// </summary>
public class Inventory
{
    public event EventHandler OnInventoryUpdate;

    private int money;
    private ArmorItem armorItem;
    private Action<Item> useItemAction;

    public Inventory(Action<Item> useItemAction)
    {
        this.useItemAction = useItemAction;
        Items = new List<Item>();
    }

    public List<Item> Items { get; set; }

    public int Money
    {
        get => money;
        set
        {
            money = value;
            OnInventoryUpdate?.Invoke(this, EventArgs.Empty);
        }
    }

    public ArmorItem ArmorItem
    {
        get => armorItem;
        set
        {
            armorItem = value;
            OnInventoryUpdate?.Invoke(this, EventArgs.Empty);
        }
    }

    /// <summary>
    /// Adds an item to the list.
    /// </summary>
    /// <param name="item">Item to add to the list.</param>
    public void AddItem(Item item)
    {
        if (item.IsStackable)
        {
            bool isItemInInventory = false;
            foreach (Item inventoryItem in Items)
            {
                if (inventoryItem.Type == item.Type)
                {
                    inventoryItem.Amount += item.Amount;
                    isItemInInventory = true;
                }
            }

            if (!isItemInInventory)
            {
                Items.Add(item);
            }
        }
        else
        {
            if (item.Type == Item.ItemType.ArmorUpgrade && !(item is ArmorItem))
            {
                ArmorItem armorItem = new ArmorItem(item.Amount, 10);
                Items.Add(armorItem);
            }
            else
            {
                Items.Add(item);
            }
        }
        OnInventoryUpdate?.Invoke(this, EventArgs.Empty);
    }

    public void RemoveItem(Item item)
    {
        if (item.IsStackable)
        {
            Item itemInInventory = null;
            foreach (Item inventoryItem in Items)
            {
                if (inventoryItem.Type == item.Type)
                {
                    inventoryItem.Amount -= item.Amount;
                    itemInInventory = inventoryItem;
                }
            }

            if (itemInInventory != null && itemInInventory.Amount <= 0)
            {
                Items.Remove(itemInInventory);
            }
        }
        else
        {
            Items.Remove(item);
        }
        OnInventoryUpdate?.Invoke(this, EventArgs.Empty);
    }

    public void UseItem(Item item)
    {
        useItemAction(item);
    }
}