﻿using UnityEngine;

public class ArmorItemSpawner : MonoBehaviour
{
    public ArmorItem armorItem;

    private void Awake()
    {
        ItemWorld.SpawnItemWorld(transform.position, armorItem.Clone() as Item);
        Destroy(gameObject);
    }
}