﻿/**
* @file InventoryBehaviour.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the InventoryBehaviour class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Behaviour to attach to the GameObject that will have the inventory (ex: the player).
/// </summary>
[RequireComponent(typeof(Collider2D))]
public class InventoryBehaviour : MonoBehaviour
{
    public InventoryUI inventoryUi;
    private PlayerControls controls;
    private Health health;
    private bool isInventoryOpened;

    public Inventory Inventory { get; private set; }

    private void Awake()
    {
        controls = new PlayerControls();
        Inventory = new Inventory(UseItem);
        // TEMP
        Inventory.Money = 200;
        health = GetComponent<Health>();
        isInventoryOpened = false;
        inventoryUi.Inventory = Inventory;

        controls.Gameplay.Inventory.performed += Inventory_performed;
        Inventory.OnInventoryUpdate += UpdateMaxArmorPoints;
    }

    private void UseItem(Item item)
    {
        if (item is ArmorItem armorItem && Inventory.ArmorItem == null)
        {
            Inventory.ArmorItem = armorItem;
            Inventory.RemoveItem(item);
        }
    }

    private void UpdateMaxArmorPoints(object sender, System.EventArgs e)
    {
        if (health == null)
            return;

        if (Inventory.ArmorItem != null)
        {
            health.MaxArmorPoints = health.maxArmorPointsParameter + Inventory.ArmorItem.ArmorValue;
        }
        else if (Inventory.ArmorItem == null)
        {
            health.MaxArmorPoints = health.maxArmorPointsParameter;
        }
    }

    /// <summary>
    /// Executes when the key to show the inventory was pressed.
    /// </summary>
    private void Inventory_performed(UnityEngine.InputSystem.InputAction.CallbackContext obj)
    {
        isInventoryOpened = !isInventoryOpened;
        inventoryUi.gameObject.SetActive(isInventoryOpened);
        Time.timeScale = isInventoryOpened ? 0f : 1f;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        ItemWorld itemWorld = collider.GetComponent<ItemWorld>();
        if (itemWorld != null)
        {
            Inventory.AddItem(itemWorld.Item);
            Destroy(collider.gameObject);
        }
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }
}