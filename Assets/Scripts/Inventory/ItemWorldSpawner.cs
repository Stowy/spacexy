﻿/**
* @file ItemWorldSpawner.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ItemWorldSpawner class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using UnityEngine;

/// <summary>
/// Component to apply on an object in the scene that will spawn an Item in the world.
/// </summary>
public class ItemWorldSpawner : MonoBehaviour
{
    public Item item;

    private void Awake()
    {
        ItemWorld.SpawnItemWorld(transform.position, new Item(item.Type, item.Amount));
        Destroy(gameObject);
    }
}