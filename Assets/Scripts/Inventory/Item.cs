﻿/**
* @file Item.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Item class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Class that represents an Item.
/// </summary>
[Serializable]
[XmlInclude(typeof(ArmorItem))]
public class Item : ICloneable
{
    const ItemType DEFAULT_ITEM_TYPE = ItemType.Silver;
    const int DEFAULT_AMOUNT = 1;

    public enum ItemType
    {
        Silver,
        Platinum,
        ArmorUpgrade
    }

    [SerializeField]
    private int amount;

    [SerializeField]
    private ItemType type;

    public Item(ItemType itemType, int amount)
    {
        Type = itemType;
        Amount = amount;
    }

    public Item() : this(DEFAULT_ITEM_TYPE, DEFAULT_AMOUNT) { }

    public int Amount { get => amount; set => amount = value; }

    public ItemType Type { get => type; set => type = value; }

    /// <summary>
    /// Returns the good sprite depending on the ItemType.
    /// </summary>
    public Sprite Sprite
    {
        get
        {
            switch (Type)
            {
                case ItemType.Silver:
                    return ItemAssets.Instance.silverSprite;

                case ItemType.Platinum:
                    return ItemAssets.Instance.platinumSprite;

                case ItemType.ArmorUpgrade:
                    return ItemAssets.Instance.armorUpgradeSprite;

                default:
                    return ItemAssets.Instance.silverSprite;
            }
        }
    }

    public bool IsStackable
    {
        get
        {
            switch (Type)
            {
                case ItemType.Silver:
                    return true;

                case ItemType.Platinum:
                    return true;

                case ItemType.ArmorUpgrade:
                    return false;

                default:
                    return true;
            }
        }
    }

    public int SellPrice
    {
        get
        {
            switch (Type)
            {
                case ItemType.Silver:
                    return 10;

                case ItemType.Platinum:
                    return 50;

                case ItemType.ArmorUpgrade:
                    return 100;

                default:
                    return 10;
            }
        }
    }

    public string Name
    {
        get
        {
            switch (Type)
            {
                case ItemType.Silver:
                    return "Silver";

                case ItemType.Platinum:
                    return "Platinum";

                case ItemType.ArmorUpgrade:
                    return "Armor Upgrade";

                default:
                    return "Silver";
            }
        }
    }

    public virtual object Clone()
    {
        return new Item(Type, amount);
    }
}