﻿/**
* @file ArmorItem.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ArmorItem class.
* @version 1.0
* @date 28.05.2020
*
* @copyright CFPT (c) 2020
*
*
*/

using System;
using System.Xml.Serialization;
using UnityEngine;

/// <summary>
/// Class for an item that can be use to upgrade the armor of the player.
/// </summary>
[Serializable]
public class ArmorItem : Item, ICloneable
{
    const int DEFAULT_AMOUNT = 1;
    const int DEFAULT_ARMOR_VALUE = 10;

    [SerializeField]
    private int armorValue;

    public ArmorItem(int amount, int armorValue) : base(ItemType.ArmorUpgrade, amount)
    {
        ArmorValue = armorValue;
    }

    public ArmorItem() : this(DEFAULT_AMOUNT, DEFAULT_ARMOR_VALUE)
    {

    }

    public int ArmorValue { get => armorValue; set => armorValue = value; }

    public override object Clone()
    {
        return new ArmorItem(Amount, ArmorValue);
    }
}