﻿/**
* @file ItemPrice.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ItemPrice class.
* @version 1.0
* @date 04.04.2020
*
* @copyright CFPT (c) 2020
*
*/

using System;
using UnityEngine;

/// <summary>
/// Class that describes the price of an item.
/// </summary>
[Serializable]
public class ItemPrice
{
    [SerializeField]
    private Item item;

    [SerializeField]
    private int price;

    public ItemPrice(Item item, int price)
    {
        Item = item;
        Price = price;
    }

    public Item Item { get => item; set => item = value; }
    public int Price { get => price; set => price = value; }
}