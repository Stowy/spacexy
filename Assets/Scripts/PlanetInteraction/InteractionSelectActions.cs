﻿/**
* @file InteractionSelectActions.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the InteractionSelectActions class.
* @version 1.0
* @date 04.04.2020
*
* @copyright CFPT (c) 2020
*
*/

using CodeMonkey.Utils;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Function to handle the interaction with the planets.
/// </summary>
public class InteractionSelectActions : MonoBehaviour
{
    public PlanetInteractionBehaviour planetInteractionBehaviour;
    public MissionBehaviour missionBehaviour;
    public float itemBuyHeight = 107f;
    public float itemSellHeight = 107f;

    /// <summary>
    /// Hide the select interaction panel.
    /// </summary>
    private void HideEverythingAndShowBackButton()
    {
        planetInteractionBehaviour.selectInteraction.SetActive(false);
        planetInteractionBehaviour.buyMenu.SetActive(false);
        planetInteractionBehaviour.sellMenu.SetActive(false);
        planetInteractionBehaviour.missionsMenu.SetActive(false);

        planetInteractionBehaviour.backButton.SetActive(true);
    }

    /// <summary>
    /// Close the interaction menu.
    /// </summary>
    public void CloseMenu()
    {
        planetInteractionBehaviour.HideInteractSelect();
    }

    /// <summary>
    /// Show the menu to sell items.
    /// </summary>
    public void ShowSellMenu()
    {
        HideEverythingAndShowBackButton();

        // Show the menu
        planetInteractionBehaviour.sellMenu.SetActive(true);

        // Show the money
        Inventory inventory = planetInteractionBehaviour.gameObject.GetComponent<InventoryBehaviour>().Inventory;
        int money = inventory.Money;
        TextMeshProUGUI moneyText = planetInteractionBehaviour.sellMenu.transform.Find("Money").GetComponent<TextMeshProUGUI>();
        moneyText.SetText(money.ToString());

        planetInteractionBehaviour.EmptySellItemsMenu();

        int y = 0;

        // Show the items to sell
        foreach (Item item in inventory.Items)
        {
            // Instantiate the item
            GameObject itemSell = Instantiate(planetInteractionBehaviour.itemSellTemplate, planetInteractionBehaviour.itemsSell.transform);
            itemSell.SetActive(true);

            ButtonUI button = itemSell.GetComponent<ButtonUI>();
            button.ClickFunc = () =>
            {
                // Sell the item
                inventory.Money += item.SellPrice;
                inventory.RemoveItem(item);
                ShowSellMenu();
            };

            // Set the position
            RectTransform rectTransform = itemSell.GetComponent<RectTransform>();
            rectTransform.anchoredPosition = new Vector2(0, y * (itemSellHeight));

            // Set the image
            itemSell.transform.Find("Image").GetComponent<Image>().sprite = item.Sprite;

            // Set the text
            itemSell.transform.Find("ItemType").GetComponent<TextMeshProUGUI>().SetText(item.Name);

            // Set the price
            itemSell.transform.Find("Price").GetComponent<TextMeshProUGUI>().SetText(item.SellPrice.ToString());

            y++;
        }
    }

    /// <summary>
    /// Show the menu to buy items.
    /// </summary>
    public void ShowBuyMenu()
    {
        HideEverythingAndShowBackButton();

        // Show the menu
        planetInteractionBehaviour.buyMenu.SetActive(true);

        // Show the money
        Inventory inventory = planetInteractionBehaviour.gameObject.GetComponent<InventoryBehaviour>().Inventory;
        int money = inventory.Money;
        TextMeshProUGUI moneyText = planetInteractionBehaviour.buyMenu.transform.Find("Money").GetComponent<TextMeshProUGUI>();
        moneyText.SetText(money.ToString());

        planetInteractionBehaviour.EmptyBuyItemsMenu();

        // Show the items to sell
        if (planetInteractionBehaviour.ItemPrices != null)
        {
            int y = 0;

            foreach (ItemPrice itemPrice in planetInteractionBehaviour.ItemPrices)
            {
                // Instantiate the item
                GameObject itemBuy = Instantiate(planetInteractionBehaviour.itemBuyTemplate, planetInteractionBehaviour.itemsBuy.transform);

                itemBuy.SetActive(true);

                ButtonUI button = itemBuy.GetComponent<ButtonUI>();
                button.ClickFunc = () =>
                {
                    // Buy the item
                    if (inventory.Money >= itemPrice.Price)
                    {
                        inventory.Money -= itemPrice.Price;
                        inventory.AddItem(itemPrice.Item.Clone() as Item);
                        ShowBuyMenu();
                    }
                };

                // Set the position
                RectTransform rectTransform = itemBuy.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2(0, y * (itemBuyHeight));

                // Set the image
                itemBuy.transform.Find("Image").GetComponent<Image>().sprite = itemPrice.Item.Sprite;

                // Set the text
                itemBuy.transform.Find("ItemType").GetComponent<TextMeshProUGUI>().SetText(itemPrice.Item.Name);

                // Set the price
                itemBuy.transform.Find("Price").GetComponent<TextMeshProUGUI>().SetText(itemPrice.Price.ToString());

                y++;
            }
        }
    }

    public void ShowMissionsMenu()
    {
        HideEverythingAndShowBackButton();

        // Show the menu
        planetInteractionBehaviour.missionsMenu.SetActive(true);

        // Update the informations
        if(planetInteractionBehaviour.MissionOnPlanet != null)
        {
            Transform missionMenuTransform = planetInteractionBehaviour.missionsMenu.transform;
            TextMeshProUGUI titleText = missionMenuTransform.Find("Title").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI descriptionText = missionMenuTransform.Find("Description").GetComponent<TextMeshProUGUI>();
            TextMeshProUGUI moneyText = missionMenuTransform.Find("MoneyReward").GetComponent<TextMeshProUGUI>();
            Mission mission = planetInteractionBehaviour.MissionOnPlanet;

            titleText.SetText(mission.Title);
            descriptionText.SetText(mission.Description);
            moneyText.SetText(mission.MoneyReward.ToString());
        }
    }

    public void AcceptMission()
    {
        planetInteractionBehaviour.missionsMenu.SetActive(false);
        Mission mission = planetInteractionBehaviour.MissionOnPlanet;
        mission.IsActive = true;
        missionBehaviour.CurrentMission = mission;
        CloseMenu();
    }
}