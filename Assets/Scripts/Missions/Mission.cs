﻿/**
* @file Mission.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the Mission class. Inspired by this video: https://www.youtube.com/watch?v=e7VEe_qW4oE
* @version 1.0
* @date 05.06.2020
*
* @copyright CFPT (c) 2020
*
*/

using System;
using UnityEngine;

/// <summary>
  /// Represents a mission that a player can do.
  /// </summary>
[Serializable]
public class Mission
{
    // Constants
    private const bool DEFAULT_IS_ACTIVE = false;
    private const string DEFAULT_TITLE = "TITLE";
    private const string DEFAULT_DESCRIPTION = "DESCRIPTION";
    private const int DEFAULT_MONEY_REWARD = 10;

    // Fields
    [SerializeField]
    private bool isActive;
    [SerializeField]
    private string title;
    [SerializeField]
    private string description;
    [SerializeField]
    private int moneyReward;
    [SerializeField]
    private MissionGoal missionGoal;

    // Constructors
    public Mission(bool isActive, string title, string description, int moneyReward, MissionGoal missionGoal)
    {
        IsActive = isActive;
        Title = title;
        Description = description;
        MoneyReward = moneyReward;
        MissionGoal = missionGoal;
    }

    public Mission() : this(DEFAULT_IS_ACTIVE, DEFAULT_TITLE, DEFAULT_DESCRIPTION, DEFAULT_MONEY_REWARD, new MissionGoal()) { }

    // Properties
    public bool IsActive { get => isActive; set => isActive = value; }
    public string Title { get => title; set => title = value; }
    public string Description { get => description; set => description = value; }
    public int MoneyReward { get => moneyReward; set => moneyReward = value; }
    public MissionGoal MissionGoal { get => missionGoal; set => missionGoal = value; }

    // Methods
    public void Complete()
    {
        isActive = false;
    }
}
