﻿using UnityEngine;
using System.Collections;

public class MissionBehaviour : MonoBehaviour
{
    public Mission CurrentMission { get; set; } = null;

    public void EnemyKilled()
    {
        Debug.Log(CurrentMission);
        if (CurrentMission != null && CurrentMission.IsActive)
        {
            CurrentMission.MissionGoal.CurrentAmount++;

            if (CurrentMission.MissionGoal.IsReached)
            {
                // Add the money to the player
                InventoryBehaviour inventory = gameObject.GetComponent<InventoryBehaviour>();
                if (inventory != null)
                {
                    inventory.Inventory.Money += CurrentMission.MoneyReward;
                }

                // Mark the mission as complete
                CurrentMission.Complete();
            }
        }
    }
}
