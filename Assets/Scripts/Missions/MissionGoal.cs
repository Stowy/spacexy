﻿using System;
using UnityEngine;

public enum GoalType
{
    Kill
}

[Serializable]
public class MissionGoal
{
    [SerializeField]
    private GoalType goalType;
    [SerializeField]
    private int requiredAmount;
    [SerializeField]
    private int currentAmount;

    public GoalType GoalType { get => goalType; set => goalType = value; }
    public int RequiredAmount { get => requiredAmount; set => requiredAmount = value; }
    public int CurrentAmount { get => currentAmount; set => currentAmount = value; }
    public bool IsReached
    {
        get
        {
            return CurrentAmount >= RequiredAmount;
        }
    }

}
