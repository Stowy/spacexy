﻿/**
 * @file PlanetDescription.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PlanetDescription class. Inspired by the script in Starja.
 * @version 1.0
 * @date 02.06.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using System;
using UnityEngine;

/// <summary>
/// MonoBehaviour to describe a planet.
/// </summary>
public class PlanetDescription : MonoBehaviour
{
    public int temperature;
    public string planetName;

    private long radius = 0; //km
    private long surface = 0; // km2
    private long volume = 0; // Millions de km3
    private long mass = 0; // Milliards de tonnes
    private long rotationSpeed = 0; // km/h

    public string Description { get; private set; }

    private void Start()
    {
        radius = Convert.ToInt64(gameObject.transform.localScale.x * 2000);
        surface = Convert.ToInt64(radius * 79972.941361f);
        volume = Convert.ToInt64(radius * 169.8353716f);
        mass = Convert.ToInt64(radius * 936594543.7441204f);
        rotationSpeed = Convert.ToInt64(radius / 3.81 - (radius - 6378) / 5) + UnityEngine.Random.Range(-Convert.ToInt32(radius / 10), Convert.ToInt32(radius / 10));

        Description = "Name : " + planetName + "\n\r";
        Description += "Temperature : " + temperature + " °C\n\r";
        Description += "Radius : " + radius + " km\n\r";
        Description += "Surface : " + surface + " km2\n\r";
        Description += "Volume : " + volume + " millions of km3\n\r";
        Description += "Mass : " + mass + " billions of tons\n\r";
        Description += "Rotation speed : " + rotationSpeed + " km/h\n\r";
    }
}