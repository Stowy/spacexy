﻿/**
 * @file SaveHandler.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the SaveHandler class.
 * @version 1.0
 * @date 04.06.2020
 *
 * @copyright CFPT (c) 2020
 *
 */
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

/// <summary>
/// Handles all the saving and loading features of the game.
/// </summary>
public class SaveHandler : Singleton<SaveHandler>
{
    private GameObject player;

    public GameObject Player 
    {
        get
        {
            if(player == null)
            {
                Player = PlayerMovementsBehaviour.Instance.gameObject;
            }

            return player;
        }
        private set => player = value; 
    }

    protected SaveHandler() { }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Save()
    {
        XmlSerializer itemsSerializer = new XmlSerializer(typeof(List<Item>));
        XmlSerializer armorItemSerializer = new XmlSerializer(typeof(ArmorItem));
        XmlSerializer missionSerializer = new XmlSerializer(typeof(Mission));

        float playerPositionX = Player.transform.position.x;
        float playerPositionY = Player.transform.position.y;

        Inventory inventory = Player.GetComponent<InventoryBehaviour>().Inventory;
        Mission mission = Player.GetComponent<MissionBehaviour>().CurrentMission;

        // Save the items in the inventory
        using (StringWriter sw = new StringWriter())
        {
            itemsSerializer.Serialize(sw, inventory.Items);
            PlayerPrefs.SetString("items", sw.ToString());
        }

        // Save the armor item
        using (StringWriter sw = new StringWriter())
        {
            armorItemSerializer.Serialize(sw, inventory.ArmorItem);
            PlayerPrefs.SetString("armorItem", sw.ToString());
        }

        using (StringWriter sw = new StringWriter())
        {
            missionSerializer.Serialize(sw, mission);
            Debug.Log(sw.ToString());
            PlayerPrefs.SetString("mission", sw.ToString());
        }

        // Save the money
        PlayerPrefs.SetInt("money", inventory.Money);

        // Save the player position
        PlayerPrefs.SetFloat("playerPositionX", playerPositionX);
        PlayerPrefs.SetFloat("playerPositionY", playerPositionY);

        Debug.Log("Game saved");
    }

    public void Load()
    {
        Inventory inventory = Player.GetComponent<InventoryBehaviour>().Inventory; 
        
        // Load the position of the player
        float playerPositionX = PlayerPrefs.GetFloat("playerPositionX", 0);
        float playerPositionY = PlayerPrefs.GetFloat("playerPositionY", 0);
        Vector2 playerPosition = new Vector2(playerPositionX, playerPositionY);
        Player.transform.position = playerPosition;

        // Load the money
        inventory.Money = PlayerPrefs.GetInt("money", 0);

        // Load the items
        XmlSerializer itemsSerializer = new XmlSerializer(typeof(List<Item>));
        List<Item> items = new List<Item>();
        string textItem = PlayerPrefs.GetString("items");

        if (textItem.Length != 0)
        {
            using (StringReader sr = new StringReader(textItem))
            {
                items = itemsSerializer.Deserialize(sr) as List<Item>;
            }
        }

        inventory.Items = items;

        // Load the armor item
        XmlSerializer armorItemSerializer = new XmlSerializer(typeof(ArmorItem));
        ArmorItem armorItem = null;
        string textArmorItem = PlayerPrefs.GetString("armorItem");

        if (textArmorItem.Length != 0)
        {
            using (StringReader sr = new StringReader(textArmorItem))
            {
                armorItem = armorItemSerializer.Deserialize(sr) as ArmorItem;
            }
        }

        inventory.ArmorItem = armorItem;

        // Load the mission
        XmlSerializer missionSerializer = new XmlSerializer(typeof(Mission));
        Mission mission = null;
        string textMission = PlayerPrefs.GetString("mission");

        if(textMission.Length != 0)
        {
            using (StringReader sr = new StringReader(textMission))
            {
                mission = missionSerializer.Deserialize(sr) as Mission;
            }
        }

        Player.GetComponent<MissionBehaviour>().CurrentMission = mission;
    }
}
