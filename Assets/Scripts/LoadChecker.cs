﻿/**
 * @file LoadChecker.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the LoadChecker class.
 * @version 1.0
 * @date 04.06.2020
 *
 * @copyright CFPT (c) 2020
 *
 */
using UnityEngine;

/// <summary>
/// Class here to check if we asked to load the settings.
/// </summary>
public class LoadChecker : MonoBehaviour
{
    private void Awake()
    {
        if(GameManager.Instance.State == GameManager.GameState.LoadSave)
        {
            SaveHandler.Instance.Load();
            GameManager.Instance.State = GameManager.GameState.InGame;
        }
    }
}
