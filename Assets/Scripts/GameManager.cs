﻿/**
 * @file GameManager.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the GameManager class.
 * @version 1.0
 * @date 28.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages the game state.
/// </summary>
public class GameManager : Singleton<GameManager>
{
    public enum GameState
    {
        MainMenu,
        LoadSave,
        InGame,
        GameOver
    }

    public float restartDelay = 1f;

    private GameState state;

    public GameState State
    {
        get => state;
        set
        {
            GameState oldState = state;
            state = value;
            if (oldState != state)
            {
                switch (state)
                {
                    case GameState.InGame:
                        StartGame();
                        break;

                    case GameState.GameOver:
                        EndGame();
                        break;

                    default:
                        break;
                }
            }
        }
    }

    protected GameManager()
    {
        // Do Nothing
    }

    public void StartGame()
    {
        Debug.Log("Start Game");
    }

    public void EndGame()
    {
        Time.timeScale = 0f;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        State = GameState.LoadSave;
        Time.timeScale = 1f;
    }
}