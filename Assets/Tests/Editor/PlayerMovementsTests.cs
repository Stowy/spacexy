﻿/**
 * @file PlayerMovementsTests.cs
 * @author Fabian Huber (fabian.hbr@eduge.ch)
 * @brief Contains the PlayerMovementsTests class.
 * @version 1.0
 * @date 25.05.2020
 *
 * @copyright CFPT (c) 2020
 *
 */

using NUnit.Framework;
using UnityEngine;

/// <summary>
/// Tests for the PlayerMovements class.
/// </summary>
public class PlayerMovementsTests
{
    [Test]
    public void CtorTest()
    {
        PlayerMovements playerMovements = new PlayerMovements(2, 4);

        Assert.AreEqual(2f, playerMovements.PlayerSpeed);
        Assert.AreEqual(4f, playerMovements.AfterburnerSpeed);
    }

    [Test]
    public void GetMovementNoAfterburnerTest()
    {
        PlayerMovements playerMovements = new PlayerMovements(2, 4);
        Vector2 initialMovement = new Vector2(10, 12);

        Vector2 finalMovement = playerMovements.GetMovement(initialMovement);

        // vector(10, 12) * (2 + 0);
        Assert.AreEqual(20, finalMovement.x);
        Assert.AreEqual(24, finalMovement.y);
    }

    [Test]
    public void GetMovementStartAfterburnerTest()
    {
        PlayerMovements playerMovements = new PlayerMovements(2, 4);
        playerMovements.StartAfterburner();
        Vector2 initialMovement = new Vector2(10, 12);

        Vector2 finalMovement = playerMovements.GetMovement(initialMovement);

        // vector(10, 12) * (2 + 4);
        Assert.AreEqual(60, finalMovement.x);
        Assert.AreEqual(72, finalMovement.y);
    }

    [Test]
    public void GetMovementStopAfterburnerTest()
    {
        PlayerMovements playerMovements = new PlayerMovements(2, 4);
        playerMovements.StartAfterburner();
        playerMovements.StopAfterburner();
        Vector2 initialMovement = new Vector2(10, 12);

        Vector2 finalMovement = playerMovements.GetMovement(initialMovement);

        // vector(10, 12) * (2 + 0);
        Assert.AreEqual(20, finalMovement.x);
        Assert.AreEqual(24, finalMovement.y);
    }
}