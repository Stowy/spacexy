﻿/**
* @file ExtentionMethodsTests.cs
* @author Fabian Huber (fabian.hbr@eduge.ch)
* @brief Contains the ExtensionMethodsTests class.
* @version 1.0
* @date 26.05.2020
*
* @copyright CFPT (c) 2020
*
*/

using Extensions;
using NSubstitute;
using NUnit.Framework;
using System;

/// <summary>
/// Tests of the ExtensionMethods class.
/// </summary>
internal class ExtensionMethodsTests
{
    [Test]
    public void NextWithPrecisionFloatTest(
        [Values(-100, 0, 1)] int min,
        [Values(int.MaxValue, 10, 10)] int max,
        [Values(10, 1000, 10000)] int precision)
    {
        Random rnd = Substitute.For<Random>();
        rnd.Next(min * precision, max * precision).Returns(min * precision);

        float value = rnd.NextFloat(min, max, precision);

        Assert.AreEqual((float)min, value);
    }

    [Test]
    public void NextWithoutPrecisionFloatTest(
        [Values(-100, 0, 1)] int min,
        [Values(int.MaxValue, 10, 10)] int max)
    {
        Random rnd = Substitute.For<Random>();
        rnd.Next(min * 1000, max * 1000).Returns(min * 1000);

        float value = rnd.NextFloat(min, max, 1000);

        Assert.AreEqual((float)min, value);
    }

    [Test, Sequential]
    public void AngleToPointTest(
        [Values(float.MinValue, float.MinValue, float.MaxValue, float.MaxValue, 10f, 18f, 0f, 0f)] float x,
        [Values(float.MinValue, float.MaxValue, float.MinValue, float.MaxValue, 10f, 15f, 1f, 0f)] float y,
        [Values(-135f, 135f, -45f, 45f, 45f, 39.805f, 90f, 0f)] float result)
    {
        UnityEngine.Vector2 vector = new UnityEngine.Vector2(0, 0);
        UnityEngine.Vector2 vector2 = new UnityEngine.Vector2(x, y);

        float angle = vector.AngleToPoint(vector2);

        Assert.AreEqual(result, angle, 0.001);
    }
}